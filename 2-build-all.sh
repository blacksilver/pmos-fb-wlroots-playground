#!/bin/sh -e
arch=$(arch | sed "s,l$,,g")
pkgdir=$(basename $PWD)
JOBS=3
# build with fb support
(cd seatd && abuild checksum && abuild -r)
sudo apk add ~/packages/$pkgdir/$arch/seatd-0.6.3-r0.apk ~/packages/$pkgdir/$arch/libseat-0.6.3-r0.apk

# build with libseat and without logind
(cd wlroots0.12 && abuild checksum && abuild -r)
sudo apk add ~/packages/$pkgdir/$arch/wlroots0.12-0.12.0-r1.apk

# build wlroots fb backend
(cd fb-wlroots && abuild checksum && abuild -r)
sudo apk add ~/packages/$pkgdir/$arch/fb-wlroots-0.12.0-r0.apk

# build patched phoc
(cd phoc && abuild checksum && abuild -r)
sudo apk add ~/packages/$pkgdir/$arch/phoc-0.9.0-r1.apk
