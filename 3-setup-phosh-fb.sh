#!/bin/sh

sudo cp phosh-fb.sh /etc/profile.d/phosh-fb.sh

sudo addgroup $USER seat

if loginctl show-seat seat0 | grep CanGraphical=no; then
  echo Your seat is not graphical.
  echo You need to add a udev rule for this, like
  echo SUBSYSTEM=="graphics", KERNEL=="fb0", DRIVERS=="s3c-fb", TAG+="master-of-seat"
fi

# We might add seatd as wanted service to /etc/init.d/tinydm instead
sudo openrc default seatd
