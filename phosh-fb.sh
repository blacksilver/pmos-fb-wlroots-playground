#!/bin/sh
#export G_MESSAGES_DEBUG=all

export WLR_BACKENDS=fbdev,libinput
export WLR_NO_HARDWARE_CURSORS=1

export LIBGL_ALWAYS_SOFTWARE=true
export GALLIUM_DRIVER=llvmpipe 
#export GALLIUM_DRIVER=softpipe
