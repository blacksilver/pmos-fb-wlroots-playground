PostmarketOS framebuffer wlroots playground
===========================================

Here are scripts and patches to build and test framebuffer wlroots backend with phoc on a device.
Tested with qemu and postmarketos-ui-phosh (phoc 0.9.0).

# wlroots

wlroots0.12 is built with libseat only as wlroots>=0.14 will always use libseat.
With upgrade to phoc >= 0.11.0 this is not needed anymore.

# fb-wlroots

The backend is only compatible with wlroots 0.12.
It must be adapted on phoc upgrade due to refactoring done from wlroots 0.13 on.

# seatd

Adding fbdev support to seatd and using it just worked and avoids patching of wlroots0.12.
users must be member of group seat for seatd/libseat
elogind via libseat is attempted after phoc/wlroots upgrade.

# Master-of-seat

If `loginctl show-seat seat0` shows `CanGraphical=no`, an udev rule should be created.
See 71-seat-mydevice.rules

# Backend selection

fbdev backend in phoc is selected within /etc/profile.d/phosh-fb.sh

